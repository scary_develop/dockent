<?php

namespace Dockent\enums;

/**
 * Class TableName
 * @package Dockent\enums
 */
abstract class TableName
{
    const NOTIFICATIONS = 'notifications';
}